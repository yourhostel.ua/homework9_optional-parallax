"use strict";
const element1 = document.querySelector('.parallax1')
const element2 = document.querySelector('.parallax2')
const element3 = document.querySelector('.parallax3')

function parallaxElement (parameter, ratio = 0.6){             //створюємо функцію з двома параметрами -
    window.addEventListener("scroll", function (){   //звернення до блоку, та коефіцієнт зміщення
            let offset = window.scrollY;                  //зчитуємо прокрутку привласнюємо offset-у
            parameter.style.backgroundPositionY = offset // методу прокручування бекграунду по 'Y'
                * ratio + "px";                        //множимо 'offset' на 'ratio' і передаємо параметр

    });                                              // який застосовується до нашого блоку 'parameter'
}


                                                      //викликаємо функцію
parallaxElement(element1,0.7);
parallaxElement(element2,0.6);
parallaxElement(element3,0.7);

//*************************************** перемикач колірних тем *****************************************************
if(!localStorage.getItem('color-theme')){localStorage.setItem('color-theme','white')}
const white = document.querySelector('.span-top-interior')
const vanilla = document.querySelector('.span-bottom-interior')

function trigger(a,b){
    document.querySelectorAll(`.${a}`).forEach((e) =>{
        e.classList.toggle(`${a}`)
        e.classList.add(`${b}`)
    })
}

function saveThem(a,b){
    switch (localStorage['color-theme']){
   case a :
        trigger('white','vanilla')
        trigger('color-white','color-vanilla')
        trigger('menu-list-white','menu-list-vanilla')
        vanilla.textContent = 'vanilla'
        white.textContent = 'vanilla'
       localStorage['color-theme'] ='vanilla'
        break;
        case b :
        trigger('vanilla','white')
        trigger('color-vanilla','color-white')
        trigger('menu-list-vanilla','menu-list-white')
        vanilla.textContent = 'white'
        white.textContent = 'white'
        localStorage['color-theme'] = 'white'
        break;
   }
}

saveThem('vanilla','white')


document.querySelector('.button').addEventListener('click',() =>{
    console.log(localStorage.getItem('color-theme'))
    saveThem('white','vanilla')
})
